import { useReducer } from "react";

const initialState = {count: 0};

function reducer(state, action) {
    console.log('update state:');
    console.log(state);
    switch (action.type) {
        case 'increment':
            return {count: state.count + 1};
        case 'decrement':
            return {count: state.count - 1};
        case 'reset':
            return initialState;
        default:
            throw new Error();
    }
}

function Counter() {
    const [state, dispatch] = useReducer(reducer, initialState);
    console.log('render:');
    console.log(state);
    return (
      <div style={{display:"none"}}>
        Count: {state.count}
        <button onClick={() => dispatch({type: 'decrement'})}>-</button>
        <button onClick={() => dispatch({type: 'increment'})}>+</button>
        <button onClick={() => dispatch({type: 'reset'})}>Reset</button>
      </div>
    );
}

export default Counter